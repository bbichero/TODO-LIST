<?hh

require("config.php");

//Include Connection class
require("class/hack/Connection.class.php");

//Instantiate the PDO object for connection
$PDO = new Connection($host_name, $db_name, $username, $password, $log_request, $debug_req, $option_pdo);

if (isset($_POST['new_item']))
{
    if ($id = $PDO->req_last_insert("query", "INSERT INTO items (value) VALUES ('" . $_POST['new_item'] . "')"))
    {
        $new_item = array('id' => $id, 'value' => $_POST['new_item'], 'status' => '', 'new_value' => $_POST['new_item'], 'select' => false);
        echo json_encode($new_item);
    }
}
else if (isset($_POST['delete_id']) && is_numeric($_POST['delete_id']) && (int)$_POST['delete_id'] > 0)
{
    if ($PDO->query("DELETE FROM items WHERE id = " . $_POST['delete_id']))
        echo $_POST['delete_id'];
}
else if (isset($_POST['update_id']) && is_numeric($_POST['update_id']))
{
    if ($PDO->query("UPDATE items SET value = '" . $_POST['new_value'] . "' WHERE id = " . $_POST['update_id']))
    {
        $update_item = array('update_id' => $_POST['update_id'], 'new_value' => $_POST['new_value']);
        echo json_encode($update_item);
    }
}
else if (isset($_POST['delete_select']) && !empty($_POST['delete_select']))
{
    $id_del = $_POST['delete_select'];

    if ($PDO->query("DELETE FROM items WHERE id IN (" . $id_del . ")") && !empty($id_del))
        echo $id_del;
}
